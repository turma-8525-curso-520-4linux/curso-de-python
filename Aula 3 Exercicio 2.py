# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

def calculadora(x, y):
    print(f"""
    Operações:
          1 - Soma
          2 - Diferença ou subtração
          3 - Multiplicação
          4 - Divisão
          """)
    opcao = input("Informe a operação: ")    
    if opcao == "1":
        print(x + y)
    elif opcao == "2":
        print(x - y)
    elif opcao == "3":
        print(x * y)
    elif opcao == "4":
        print(x / y)
    else:
        print(f"Opção inválida.")

print(f"Vamos iniciar nossa calculadora!")
x = float(input("Informe o primeiro número: "))
y = float(input("Informe o segundo número: "))
calculadora(x,y)

# Resolucão do professor

# def calculadora(valor1, valor2, operacao):
#     if operacao == "1":
#         return(f"O resultado da soma entre {valor1} e {valor2} é {valor1 + valor2}")
#     elif operacao == "2":
#         return(f"O resultado da diferença entre {valor1} e {valor2} é {valor1 - valor2}")
#     elif operacao == "3":
#         return(f"O resultado da multiplicação entre {valor1} e {valor2} é {valor1 * valor2}")
#     elif operacao == "4":
#         if valor2 == 0.0:
#             return("Não é possível dividir um número por zero.")
#         else:
#             return(f"O resultado da multiplicação entre {valor1} e {valor2} é {valor1 / valor2:.2f}")
#     else:
#         return("O valor indigado para a operação é inválido")

# n1 = float(input("Digite o primeiro número: "))
# n2 = float(input("Digite o segundo número: "))

# escolha = input(f"""
#             Escolha uma das opções a seguir:
#                 1 - Soma
#                 2 - Subtração
#                 3 - Multiplicação
#                 4 - Divisão
#                 """)

# print(calculadora(n1, n2, escolha))