from random import choice, randint
from time import sleep
from funcoes import definirCategoria


# 3) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

# lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
# "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
# "Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.

# Não consegui resolver a tempo

# Resolução do professor

lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.

numero_part = int(input("Quantas pessoas vão participar do sorteio? [2~20]: "))
n_sorteios = int(input("Quantas pessoas serão sorteadas? "))

participantes = []

for loop in range(0, numero_part):
    escolhido = choice(lista)
    participantes.append(escolhido)
    lista.remove(escolhido)

print(f"Os participantes serão: {participantes}")

for sorteio in range(0, n_sorteios):
    ganhador = choice(participantes)
    print(f"{ganhador} venceu o sorteio!")
    participantes.remove(ganhador)
