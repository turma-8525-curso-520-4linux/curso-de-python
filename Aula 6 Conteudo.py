# CASE / MATCH 
#  Entrou no Python a partir da versão 3.10

nome = input("Digite um nome: ")

match nome:
    case "Kaeno":
        print("Caramba, que belo nome.")

    case "Allan":
        print("Que nome bonito também!")

    case "João":
        print("Que nome comum.")

    case _:
        print("Não conheço esse nome.")

ESCOPO/SCOPE 
        
x = 300 # Variável Global - Escopo Global

def funcao():
    global x
    x = 250 # Variável Local - Escopo Local
    print(x)

funcao()
print(x)


# CLASSES/CLASS
# As variáveis com duplo underline "__xxx" é para não chamar a variável de fora da classe
# Classes devem estar separadas por duas linhas

class Pilha:

    def __init__(self):
        self.__pilha = []
        self.__topo = 0

    def empilhar(self, item):
        self.__pilha.append(item)
        self.__topo += 1

    def desempilhar(self):
        if self.__topo > 0:
            ultimo_item = self.__pilha[-1] #saber último número da lista
            self.__pilha.remove(ultimo_item)
            self.__topo -= 1
            return "Item removido."
        else:
            return "Nenhum item empilhado."
        
    def checar(self):
        return self.__pilha
        
balcao = Pilha()

balcao.empilhar("Prato de vidro verde")
balcao.empilhar("Prato de porcelanda azul")
balcao.empilhar("Prato de metal")
balcao.empilhar("Prato de madeira")

balcao.desempilhar()
balcao.desempilhar()


# # Encapsulamento

print(balcao.checar())


# HERANÇA

class Funcionario: #Classe principal
    def __init__(self):
        self.__nome = ""
        self.__idade = 0
        self.__salario = 0

class Gerentes(Funcionario): #Classe herdeira
    def __init__(self):
        super().init__()
        self.__bonus = "25%"


# POLIMORFISMO

class Cliente:
    def __init__(self):
        self.carrinho = []
        self.cpf = ''
        self.total = 0

    def adicionar_item(self, item):
        self.carrinho.apend(item)

    def caixa(self):
        for item in self.carrinho:
            self.total += 1.99
        return self.total

class ClienteVIP(Cliente):
    def __init__(self):
        super().__init__()
        self.desconto = 0.95

    def caixa(self):
        for item in self.carrinho:
            self.total += 1.99
        return self.total * self.desconto