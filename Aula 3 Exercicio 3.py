import os

# Exercicio 3:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações 
# em funções.

    # 3) Escreva um script em python que represente uma quitanda. O seu programa deverá
    # apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
    # deverá ser adicionada a uma cesta de compras.

    # Menu principal:

    # Quitanda:
    # 1: Ver cesta
    # 2: Adicionar frutas
    # 3: Sair

    # Menu de frutas:
    # Digite a opção desejada:
    # Escolha fruta desejada:
    # 1 - Banana
    # 2 - Melancia
    # 3 - Morango

    # Digite à opção desejada: 2
    # Melancia adicionada com sucesso!

    # Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
    # Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
    # Digitado uma opção inválida

    # O programa deverá ser encerrado apenas se o usuário digitar a opção 3.

# cesta = []
# valor_total = 0

# def ver_cesta():
#     if len(cesta) < 1:
#             print("Sua cesta está vazia.")
#     else:
#         print(cesta)
#         print(valor_total)

# def menu_frutas():
#      while True:
#             print("""
#                 Menu de frutas:
#                 Digite a opção desejada:
#                 Escolha fruta desejada:
#                 1 - Banana    R$ 2.50
#                 2 - Melancia  R$ 8.00
#                 3 - Morango   R$ 6.00
#                 """)
            
#             opcao_fruta = input("Selecione a fruta para adicionar na cesta: ")

#             if opcao_fruta == "1":
#                 cesta.append("Banana")
#                 valor_total = valor_total + 2.50
#                 # valor_total += 2.50 # forma simplificada
#                 print("Banana foi adicionada á cesta.")
#                 break
#             elif opcao_fruta == "2":
#                 cesta.append("Melancia")
#                 valor_total = valor_total + 8.00
#                 # valor_total += 8.00 # forma simplificada
#                 print("Melancia foi adicionada á cesta.")
#                 break
#             elif opcao_fruta == "3":
#                 cesta.append("Morango")
#                 valor_total = valor_total + 6.00
#                 # valor_total += 6.00 # forma simplificada
#                 print("Morango foi adicionada á cesta.")
#                 break
#             else:
#                 print("Valor inválido. Digite novamente.")

# def sair():
#      print(f"""Conteúdo da ceta: {cesta}
#                   Valor gasto: {valor_total} 
#                   Fim do programa.
#               """)
     
# while True:    
#     print(f"""
# Menu Principal: 
# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair
          
# """)
#     opcao = input("Digite a opção desejada: ")
#     if opcao == "1":
#          #função ver_cesta

#     elif opcao == "2":
#         #função menu_frutas
        
#     elif opcao == "3":
#         #função sair        
#         break
#     else:
#         print("Valor inválido. Digite novamente.")

# Resolução do Professor

cesta = []
valor_gasto = 0

def verCesta():
    if len(cesta) < 1:
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print("A cesta está vazia")
    else:
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print(cesta)

def menuFrutas():
    global valor_gasto
    while True:
        print("""
        Menu de frutas:
        Escolha fruta desejada:
        1 - Banana   R$2.50
        2 - Melancia R$8.00
        3 - Morango  R$6.00
            
        """)

        opcao_fruta = input("Selecione a fruta para adicionar na cesta. ")

        if opcao_fruta == "1":
            cesta.append("Banana")
            valor_gasto = valor_gasto + 2.50
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("'Banana' foi adicionada a cesta.")
            break
        elif opcao_fruta == "2":
            cesta.append("Melancia")
            valor_gasto = valor_gasto + 8.00
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("'Melancia' foi adicionada a cesta.")
            break
        elif opcao_fruta == "3":
            cesta.append("Morango")
            valor_gasto = valor_gasto + 6.00
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("'Morango' foi adicionada a cesta.")
            break
        else:
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("Valor inválido, digite novamente.")

def checkout():
    print(f"""Conteúdo da cesta: {cesta}
Valor gasto: R${valor_gasto:.2f}
""")

while True:
    print("""
        Quitanda:
        1: Ver cesta
        2: Adicionar frutas
        3: Sair

        """)

    opcao = input("Selecione uma das opções acima. ")

    if opcao == "1":
        verCesta()

    elif opcao == "2":
        menuFrutas()

    elif opcao == "3":
        checkout()
        break

    else:
        print("Valor inválido, digite novamente.")
