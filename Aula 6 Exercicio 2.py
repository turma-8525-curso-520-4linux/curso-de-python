# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

# Não consegui resolver a tempo

# Resolução do professor

class Fila:

    def __init__(self):
        self.__fila = []
        self.__prioridade = []

    def adicionar(self, nome, idade):
        if idade >= 65:
            self.__prioridade.append(nome)
            self.__fila.append(nome)
            return(f"{nome} entrou na fila")
        else:
            self.__fila.append(nome)
            return(f"{nome} entrou na fila")
        
    def atender(self):
        if len(self.__fila) < 1:
            return("Não há ninguém na fila.")
        else:
            atendido = self.__fila[0]
            if self.__fila[0] in self.__prioridade:
                self.__prioridade.remove(self.__fila[0])
            self.__fila.remove(self.__fila[0])
            return(f"Atendendo a primeira pessoa da fila, {atendido}")

    def dar_prioridade(self):
        if len(self.__prioridade) < 1:
            return("Não há ninguém pra dar prioridade.")
        else:
            prioritario = self.__prioridade[0]
            self.__prioridade.remove(prioritario)
            self.__fila.remove(prioritario)
            self.__fila.insert(0, prioritario)
            return(f"{prioritario} recebeu prioridade e está na frente da fila.")
        
banco = Fila()

print(banco.adicionar("Tiago", 27))
print(banco.adicionar("Caio", 28))
print(banco.adicionar("Kaique", 30))
print(banco.adicionar("Leonardo", 20))
print(banco.adicionar("Amanda", 78))
print(banco.adicionar("Maria", 66))
print(banco.adicionar("José", 12))

print(banco.atender())
print(banco.atender())
print(banco.dar_prioridade())
print(banco.atender())
