# O __xxx___ chama-se dunder e é usado para automatizar a execução do código

def final():
    print("Finalizando o teste.")

def main():
    print("Iniciando o teste.")
    final()

if __name__ == "__main__":
    main()