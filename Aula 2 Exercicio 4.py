# 4) Altere o exercicio numero 3 para adicionar o preço dos itens comprados, mantendo uma conta do valor
# total gasto nas compras, e no fim, imprima o valor total e os itens na cesta de compras.

cesta = []
valor_total = 0

while True:
    
    # código que usa o pacote os, importado no começo.
    # numa linha: os.system('cls' if os.name == 'nt' else 'clear')
    # if os.name == "nt": 
    #     os.system("cls")
    # else:
    #     os.system("clear")
    
    print(f"""
Menu Principal: 
Quitanda:
1: Ver cesta
2: Adicionar frutas
3: Sair
          
""")
    opcao = input("Digite a opção desejada: ")
    if opcao == "1":
        if len(cesta) < 1:
            print("Sua cesta está vazia.")
        else:
            print(cesta)
            print(valor_total)
    elif opcao == "2":
        while True:
            print("""
                Menu de frutas:
                Digite a opção desejada:
                Escolha fruta desejada:
                1 - Banana    R$ 2.50
                2 - Melancia  R$ 8.00
                3 - Morango   R$ 6.00
                """)
            
            opcao_fruta = input("Selecione a fruta para adicionar na cesta: ")

            if opcao_fruta == "1":
                cesta.append("Banana")
                valor_total = valor_total + 2.50
                # valor_total += 2.50 # forma simplificada
                print("Banana foi adicionada á cesta.")
                break
            elif opcao_fruta == "2":
                cesta.append("Melancia")
                valor_total = valor_total + 8.00
                # valor_total += 8.00 # forma simplificada
                print("Melancia foi adicionada á cesta.")
                break
            elif opcao_fruta == "3":
                cesta.append("Morango")
                valor_total = valor_total + 6.00
                # valor_total += 6.00 # forma simplificada
                print("Morango foi adicionada á cesta.")
                break
            else:
                print("Valor inválido. Digite novamente.")
    elif opcao == "3":
        print(f"""Conteúdo da ceta: {cesta}
                  Valor gasto: {valor_total} 
                  Fim do programa.
              """)
        break
    else:
        print("Valor inválido. Digite novamente.")