# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.

# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:

# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'

# minha primeira resolução
def saudacao(nome):
    print(f"Olá {nome}! Tudo bem com você?")
    return 
nome = input("Informe um nome: ")
saudacao(nome)

# resolucão do professor 1
def saudacao(nome):
    print(f"Olá {nome}! Tudo bem com você?")
nome = input("Informe um nome: ")
saudacao(nome)

# resolucão do professor 2
def saudacao(nome):
    return f"Olá {nome}! Tudo bem com você?"
nome = input("Informe um nome: ")
print(saudacao(nome))

# Resolucão do professor

# def saudacao(nome):
#     return(f"""
# Chamada da função: saudação('{nome}')
# Saída: 'Olá {nome}! Tudo bem com voce?'
# """)

# resp = input("Qual é o seu nome? ")
# print(saudacao(resp))