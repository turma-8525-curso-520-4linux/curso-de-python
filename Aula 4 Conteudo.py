# IMPORTAÇÃO
# Importar de outro arquivo
# Sempre no topo da tela, início do código

# Módulos nativos são os que já vem com o python mas não estão ativos
import os
import math

# Módulo random
import random

random.choice()
random.random(1, 100)
random.randint(1, 100)

# Importar funções específicas de um módulo
from os import system
system()

# Importar funções específicas de um módulo e renomea-la
from os import system as st 
st()

# Módulos baixados através do PIP (gerenciador de pacotes através da internet)
# No terminal digitar: pip install flask ou pip3 install flask

# import app # no início do código
# app.final() # exemcplo de invocação

import Aula_4_Dunder
Aula_4_Dunder.main() # exemplo de invocação