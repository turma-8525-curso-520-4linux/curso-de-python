# Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2

print("Exercício 2")

num1 = int(input("Digite o primeiro número: "))
num2 = int(input("Digite o segundo número: "))
soma = num1 + num2
dif = num1 - num2
print("-----------------------------")
print(f"Soma: {num1} + {num2} = {soma}")
print(f"Diferença: {num1} - {num2} = {dif}")