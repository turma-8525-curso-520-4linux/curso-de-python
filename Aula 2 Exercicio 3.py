import os
# Módulo para limpart a tela

# 3) Escreva um script em python que represente uma quitanda. O seu programa deverá
# apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
# deverá ser adicionada a uma cesta de compras.

# Menu principal:

# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair

# Menu de frutas:
# Digite a opção desejada:
# Escolha fruta desejada:
# 1 - Banana
# 2 - Melancia
# 3 - Morango

# Digite à opção desejada: 2
# Melancia adicionada com sucesso!

# Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
# Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
# Digitado uma opção inválida

# O programa deverá ser encerrado apenas se o usuário digitar a opção 3.

cesta = []

while True:
    
    # código que usa o pacote os, importado no começo.
    # numa linha: os.system('cls' if os.name == 'nt' else 'clear')
    # if os.name == "nt": 
    #     os.system("cls")
    # else:
    #     os.system("clear")
    
    print(f"""
Menu Principal: 
Quitanda:
1: Ver cesta
2: Adicionar frutas
3: Sair
          
""")
    opcao = input("Digite a opção desejada: ")
    if opcao == "1":
        if len(cesta) < 1:
            print("Sua cesta está vazia.")
        else:
            print(cesta)
    elif opcao == "2":
        while True:
            print("""
                Menu de frutas:
                Digite a opção desejada:
                Escolha fruta desejada:
                1 - Banana    R$ 2.50
                2 - Melancia  R$ 8.00
                3 - Morango   R$ 6.00
                """)
            
            opcao_fruta = input("Selecione a fruta para adicionar na cesta: ")

            if opcao_fruta == "1":
                cesta.append("Banana")
                print("Banana foi adicionada á cesta.")
                break
            elif opcao_fruta == "2":
                cesta.append("Melancia")
                print("Melancia foi adicionada á cesta.")
                break
            elif opcao_fruta == "3":
                cesta.append("Morango")
                print("Morango foi adicionada á cesta.")
                break
            else:
                print("Valor inválido. Digite novamente.")
    elif opcao == "3":
        print(f"""Conteúdo da ceta: {cesta}
                  Fim do programa.
              """)
        break
    else:
        print("Valor inválido. Digite novamente.")