# 3) Implemente uma função de consulta no programa do exercício 2.

# Não consegui resolver a tempo

# Resolução do professor

import csv

def consulta():
    resposta = int(input("""Qual valor deseja consultar?
0 - CPF
1 - Nome 
"""))
    
    valor = input("Digite o CPF/Nome a consultar: ")

    with open("banco.csv", "r") as arquivo:
        conteudo = csv.reader(arquivo, delimiter=";")
        for linha in conteudo:
            if linha[resposta] == valor:
                print(f"""Cadastro encontrado:
CPF: {linha[0]}
NOME: {linha[1]}
IDADE: {linha[2]}
SEXO: {linha[3]}
ENDEREÇO: {linha[4]}""")
                
while True:
    opcao = input("""Digite uma opção:
1 - Consultar um valor no banco.
2 - Cadastrar um valor no banco.
3 - Parar o programa.""")

    if opcao == "3":
        print("Parando a execução")
        break
    
    elif opcao == "2":
        cpf = input("Qual o CPF da pessoa? ")
        nome = input("Qual o nome da pessoa? ")
        idade = input("Qual o idade da pessoa? ")
        sexo = input("Qual o sexo da pessoa? ")
        endereco = input("Qual o endereço da pessoa? ")

        pessoa = (cpf, nome, idade, sexo, endereco)

        with open("banco.csv", "a+", newline="") as arquivo:
            conteudo = csv.writer(arquivo, delimiter=";")
            conteudo.writerow(pessoa)

        print(f"'{nome}' cadastrada no banco com sucesso.")

    elif opcao == "1":
        consulta()

    else:
        print("O valor digitado não é válido.")
