# 2) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z

ano_nasc = int(input("Informe seu ano de nascimento (aaaa): "))
print(f"O ano de nascimento informado foi {ano_nasc}")

if ano_nasc <= 1964:
    print(f"Voce nasceu em {ano_nasc} e portanto sua geração é baby boomer.")
elif ano_nasc <= 1979:
    print(f"Voce nasceu em {ano_nasc} e portanto sua geração é X.")
elif ano_nasc <= 1994:
    print(f"Voce nasceu em {ano_nasc} e portanto sua geração é Y.")
else:
    print(f"Voce nasceu em {ano_nasc} e portanto sua geração é Z.")