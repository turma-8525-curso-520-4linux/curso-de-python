# 1) Em muitos programas, nos é solicitado o preenchimento de algumas informações como
# nome, CPF, idade e unidade federativa. Escreva um script em Python que solicite as
# informações cadastrais mencionadas e que em seguida as apresente da seguinte forma:

# -----------------------------
# Confirmação de cadastro:
# Nome: Guido Van Rossum
# CPF: 999.888.777/66
# Idade: 65
# -----------------------------

print("Exercício 1")

nome = input("Informe seu nome: ")
cpf = input("Informe seu CPF (xxx.xxx.xxx-xx): ")
idade = input("Informe sua idade: ")

print("-----------------------------")
print("Confirmação de cadastro:")
print(f"Nome: {nome}")
print(f"CPF: {cpf}")
print(f"Idade: {idade}")
print("-----------------------------")

# Outra forma de imprimir o mesmo resultado sem repetir comandos
print(f"""
-----------------------------
Nome:  {nome}
CPF: {cpf}
Idade: {idade}
-----------------------------
""")

# Resposta do Danilinho
print (f"Confirmação de cadastro:\n", f"Nome: {nome}\n", f"CPF: {cpf}\n", f"Idade: {idade}")