import os
import math

# Exercicio 5:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.

# Não consegui resolver a tempo

# Resolução do professor

def tinta(x, y):
    area = x * y
    latas = area / 3

    return(f"Seriam necessárias {math.ceil(latas)} latas de tinta para pintar essa parede.")

altura = float(input("Qual a altura da parede em metros? "))
largura = float(input("Qual a largura da parede em metros? "))

print(tinta(altura, largura))
