# 2) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço

# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;

# Não consegui resolver a tempo

# Resolução do professor

import csv


while True:
    opcao = input("Gostaria de cadastrar uma nova entrada no banco? [S/N]")

    if opcao.lower() == "n":
        print("Parando a execução")
        break
    
    elif opcao.lower() == "s":
        cpf = input("Qual o CPF da pessoa? ")
        nome = input("Qual o nome da pessoa? ")
        idade = input("Qual o idade da pessoa? ")
        sexo = input("Qual o sexo da pessoa? ")
        endereco = input("Qual o endereço da pessoa? ")

        pessoa = (cpf, nome, idade, sexo, endereco)

        with open("banco.csv", "a+", newline="") as arquivo:
            conteudo = csv.writer(arquivo, delimiter=";")
            conteudo.writerow(pessoa)

        print(f"'{nome}' cadastrada no banco com sucesso.")

    else:
        print("Por favor, digite apenas 'S' ou 'N'.")
