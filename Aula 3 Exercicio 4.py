# Exercicio 4:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

# Não consegui resolver a tempo

# Resolução do professor

def pares(lista):
    if len(lista) < 1:
        return("A lista está vazia.")
    
    numeros_pares = 0

    for x in lista:
        if x % 2 == 0:
            numeros_pares = numeros_pares + 1 # ou numeros_pares +=1

    return(f"{numeros_pares} numeros pares foram encontrados na lista.")    

numeros = []

while True:
    valor = int(input("Escolha um número para adicionar a lista, ou digite 999 para fechar."))
    if valor == 999:
        break
    numeros.append(valor)

print(pares(numeros))